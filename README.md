MAVLink message definitions v1.0
================================

This is a drop-in replacement for MAVLink [message definitions](https://github.com/mavlink/mavlink/tree/master/message_definitions/v1.0)
without unnecessary content of `mavlink` repository. It could be useful for those who wants to use Git
submodules but doesn't want to include things which are not related to XML definitions.

The repository is updated from the upstream each midnight (GMT).
